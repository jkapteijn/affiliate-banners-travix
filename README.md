Affiliate Banners Tool - Travix
======
## To deploy

_Note: Running `npm install` will not be neccesarry as all node-modules are located in the repo. The choice for doing so, is to guarantee compatibility and because of known issues on some modules._

### 1. Set the rootPath
Before deploying, make sure to modify the rootPath to match the hosting location. _(default: http://affilaite.travix.com/)_

The `rootPath` is set in the **application-config.json** file located in the root. Grunt uses the `rootPath` to set the asset paths by doing serveral replacements.

### 2. Build the deployment package
Run `grunt deploy` to build the deployment-package. An archive called **affiliateBanners.zip** will be written to the /export/ directory.

### 3. Install
To deploy the Affiliate Banner Tool, extract the files from the **affiliateBanners.zip** to the location defined at step 1. previously.

# All Done!

---

## Prerequisite
### CROSS - Cross-Origin Resource Sharing
Make sure the install location has the CROSS headers defined. This will allow the assets to be served to the affiliate banners, while hosted at a different location.