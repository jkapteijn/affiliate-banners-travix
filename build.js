({
  mainConfigFile : "assets/js/app.js",
  baseUrl: 'assets/js',
  out: 'build/qssdk.js',
  optimize: 'uglify2',
  name: 'app',
  wrap: true,
  removeCombined: true,
  findNestedDependencies: true,
  preserveLicenseComments: false,
  logLevel: 4
})