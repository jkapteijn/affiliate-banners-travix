$(document).ready(function() {

  $('.BASEURL').text(window.location.href);

  var autoCompleteURL = '//api.cheaptickets.nl/lookup/airports/';
  var lang = 'nl-nl';

  var createList = function(element) {
    var list = $('<div class="awesomplete" data-for="'+element.attr('id')+'"><ul></ul></div>');
    list.insertAfter(element);
    list.hide();
    return list;
  };

  var getList = function(element) {
    return $('[data-for="'+element.attr('id')+'"]');
  };

  var autoComplete = function(field, hiddenField, callback) {
    if (field.val().length >= 3) {
      $.getJSON(autoCompleteURL + '?culture='+lang+'&search=' + field.val(), {}, function(data) {
        callback(data);
      });
    }
    return this;
  };

  var doRefresh = function() {
    var output = '<div class="qs-banner"';

    $('form#config input[type="text"], form#config input[type="hidden"], form#config input:checked').each(function(index, element) {

      $('.qs-banner').data($(element).attr('name'), $(element).val());

      output += ' data-' + $(element).attr('name') + '="' + $(element).val() + '"';

    });
    output += '></div>';
    $('pre#output').text(output);
    if (window.QSSDK) {
      window.QSSDK.render();
    }
  };

  createList($('#from'));
  createList($('#to'));

  $('#from,#to').on('keyup', function(e) {
    var _this = this;

    autoComplete($(e.target), $($(e.target).data('target')), function(items) {
      var list = getList($(e.target));
      list.find('ul').empty();

      items.forEach(function(row) {
        list.find('ul').append('<li data-code="'+row.Code+'" data-field="#'+$(e.target).attr('id')+'" data-target="'+$(e.target).data('target')+'">'+row.DisplayName+'</li>');
      });

      $('.awesomplete li').on('click', function(e) {
        var field = $(e.target).data('field');
        var hiddenField = $(e.target).data('target');
        $(field).val($(e.target).text());
        $(hiddenField).val($(e.target).data('code'));
        getList($(field)).hide();
        doRefresh();
      });
      list.show();
    });
  });

  $('form#config input').on('change', function(e) {
    doRefresh();
  });
});
