require.config({
  paths: {
    jquery: 'libs/jquery/dist/jquery',
    underscore: 'libs/underscore/underscore',
    backbone: 'libs/backbone/backbone',
    text: 'libs/requirejs-text/text',
    moment: 'libs/moment/moment',
    pikaday: 'libs/pikaday/pikaday',
    awesomplete: 'libs/awesomplete/awesomplete'
  },
  shim: {
    pikaday: {
      deps: ['moment']
    },
    awesomplete:  {
      exports: 'Awesomplete'
    }
  },
  map: {
    '*': {
      'css': 'libs/require-css/css'
    }
  }
});

require(['jquery', 'backbone', 'controllers/banner'], function($, Backbone, BannerController) {
  window.QSSDK = new BannerController();
  window.qsAsyncInit();
});
