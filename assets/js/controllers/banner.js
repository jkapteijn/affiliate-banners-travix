define(['jquery', 'backbone', 'underscore', 'views/banner'], function($, Backbone, _, BannerView) {
  return Backbone.View.extend({
    init: function(options) {

      this.options = $.extend({
        'baseURL' : 'http://[rootPath]',//'//travix.imediaworks.nl', //'//affiliate.travix.com', //'//banners.cheaptickets.nl', //'//www.76interactive.com/demo/travix', //'//localhost/travix',
        'element' : '.qsbanner', // can be more than 1
        'lang'		: 'nl-nl',
        'layout'  : 'leaderboard', // banner, leaderboard, vierkant, grote-rechthoek, geintegreerde-rechthoek, skyscraper, brede-skyscraper
        'label'   : 'cheaptickets-nl',
        'dateFormat' : 'DD-MM-YYYY',
        'affiliateurl' : false,
        'homepageURL' : '#',
        'maxNextDays' : 200,
        'minNextDays' : 1,
        'autoCompleteURL' : '//api.budgetair.nl/lookup/airports',
        'config' : {
          'cheaptickets-nl' : {
            'postURL' : 'http://www.cheaptickets.nl/',
            'homepageURL' : '//www.cheaptickets.nl',
            'autoCompleteURL' : '//api.cheaptickets.nl/lookup/airports',
            'params' : {
              'triptype' : {
                'name' : 'oneway',
                'values' : [
                  0, 1
                ]
              },
              'from': 'dep_apname',
              'to': 'des_apname',
              'dep_apcode' : 'dep_apcode',
              'des_apcode' : 'des_apcode',
              'vertrekdate' : 'vertrekdate',
              'retourdate' : 'retourdate',
              'nr_adults' : 'nr_adults',
              'nr_children' : 'nr_children',
              'nr_infant' : 'nr_infant',
              'autosubmit': 'autosubmit',
              'search' : 'Zoeken',
              'postDateFormat':'YYYYMMDD'
            },
            'hidden' : {
              'p_type': 'F',
              '__lanid' : 1043
            },
            'dateFormat' : 'DD-MM-YYYY',
            'strings' : {
              'retour' : 'Retour',
              'single' : 'Enkele reis',
              'from' : 'Van',
              'to' : 'Naar',
              'departure' : 'Heen',
              'arrival' : 'Terug',
              'adults' : 'Volw.',
              'kids' : 'Kinderen 2-11 jaar',
              'babies' : 'Baby\'s 0-1 jaar',
              'search' : 'Zoeken'
            }
          },
          'cheaptickets-be' : {
            'postURL' : 'http://www.cheaptickets.be/',
            'homepageURL' : '//www.cheaptickets.be',
            'dateFormat' : 'DD.MM.YYYY',
            'autoCompleteURL' : '//api.cheaptickets.be/lookup/airports',
            'params' : {
              'triptype': {
                'name': 'oneway',
                'values': [
                  0, 1
                ]
              },
              'from': 'dep_apname',
              'to': 'des_apname',
              'dep_apcode': 'dep_apcode',
              'des_apcode': 'des_apcode',
              'vertrekdate': 'vertrekdate',
              'retourdate': 'retourdate',
              'nr_adults': 'nr_adt',
              'nr_children': 'nr_children',
              'nr_infant': 'nr_infant',
              'autosubmit': 'autosubmit',
              'search': 'Zoeken',
              'postDateFormat':'DD/MM/YYYY'
            },
            'strings' : {
              'retour' : 'Retour',
              'single' : 'Enkele reis',
              'from' : 'Van',
              'to' : 'Naar',
              'departure' : 'Heen',
              'arrival' : 'Terug',
              'adults' : 'Volw.',
              'kids' : 'Kinderen 2-11 jaar',
              'babies' : 'Baby\'s 0-1 jaar',
              'search' : 'Zoeken'
            }
          },
          'cheaptickets-de' : {
            'postURL' : 'http://buchung.cheaptickets.de/checkout/flightresults',
            'homepageURL' : '//www.cheaptickets.de',
            'autoCompleteURL' : '//api.cheaptickets.de/lookup/airports',
            'params' : {
              'triptype' : {
                'name' : 'oneway',
                'values' : [
                  0, 1
                ]
              },
              'dep_apcode' : 'out0_dep',
              'des_apcode' : 'out0_arr',
              'vertrekdate' : 'out0_date',
              'retourdate' : 'in0_date',
              'nr_adults' : 'adt',
              'nr_children' : 'chd',
              'nr_infant' : 'inf',
              'search' : 'Suchen',
              'postDateFormat':'DD/MM/YYYY'
            },
            'hidden' : {
              'p_type': 'F',
              '__lanid' : 1043
            },
            'dateFormat' : 'DD.MM.YYYY',
            'strings' : {
              'retour': 'Hin-und R�ckflug ',
              'single': 'Nur Hinflug',
              'from': 'Von',
              'to': 'Nach',
              'departure': 'Hinflug',
              'arrival': 'Ruckflug',
              'adults': 'Erwachsene',
              'kids': 'Kinder (2-11)',
              'babies': 'Kleinkinder (0-1)',
              'search': 'Suchen'
            }
          },
          'cheaptickets-ch' : {
            'postURL' : 'http://www.cheaptickets.ch/checkout/flightresults',
            'homepageURL' : '//www.cheaptickets.ch',
            'autoCompleteURL' : '//api.cheaptickets.ch/lookup/airports',
            'params' : {
              'triptype' : {
                'name' : 'oneway',
                'values' : [
                  0, 1
                ]
              },
              'dep_apcode' : 'out0_dep',
              'des_apcode' : 'out0_arr',
              'vertrekdate' : 'out0_date',
              'retourdate' : 'in0_date',
              'nr_adults' : 'adt',
              'nr_children' : 'chd',
              'nr_infant' : 'inf',
              'search' : 'Suchen'
            },
            'hidden' : {
              'p_type': 'F',
              '__lanid' : 1031
            },
            'dateFormat' : 'DD.MM.YYYY',
            'strings' : {
              'retour': 'Hin-und R�ckflug ',
              'single': 'Nur Hinflug',
              'from': 'Von',
              'to': 'Nach',
              'departure': 'Hinflug',
              'arrival': 'Ruckflug',
              'adults': 'Erwachsene',
              'kids': 'Kinder (2-11)',
              'babies': 'Kleinkinder (0-1)',
              'search': 'Suchen',
              'postDateFormat':'YYYYMMDD'
            }
          },
          'budget-air-nl' : {
            'postURL' : '//www.budgetair.nl/checkout/flightresults',
            'homepageURL' : '//www.budgetair.nl',
            'autoCompleteURL' : '//api.budgetair.nl/lookup/airports',
            'params' : {
              'triptype' : {
                'name' : 'oneway',
                'values' : [
                  0, 1
                ]
              },
              'dep_apcode' : 'out0_dep',
              'des_apcode' : 'out0_arr',
              'vertrekdate' : 'out0_date',
              'retourdate' : 'in0_date',
              'nr_adults' : 'adt',
              'nr_children' : 'chd',
              'nr_infant' : 'inf',
              'search' : 'Zoeken',
              'postDateFormat':'YYYYMMDD'
            },
            'hidden' : {
              'p_type': 'F',
              '__lanid' : 1043
            },
            'dateFormat' : 'DD-MM-YYYY',
            'strings' : {
              'retour': 'Retour',
              'single': 'Enkele reis',
              'from': 'Van',
              'to': 'Naar',
              'departure': 'Heen',
              'arrival': 'Terug',
              'adults': 'Volw.',
              'kids': 'Kinderen 2-11 jaar',
              'babies': 'Baby\'s 0-1 jaar',
              'search': 'Zoeken'
            }
          },
          'budget-air-uk' : {
            'postURL' : '//www.budgetair.co.uk/checkout/flightresults',
            'homepageURL' : '//www.budgetair.co.uk',
            'autoCompleteURL' : '//api.budgetair.co.uk/lookup/airports',
            'params' : {
              'triptype' : {
                'name' : 'oneway',
                'values' : [
                  0, 1
                ]
              },
              'from': 'from',
              'to': 'to',
              'dep_apcode' : 'out0_dep',
              'des_apcode' : 'out0_arr',
              'vertrekdate' : 'vertrekdate',
              'retourdate' : 'retourdate',
              'nr_adults' : 'adt',
              'nr_children' : 'chd',
              'nr_infant' : 'inf',
              'search' : 'Search',
              'postDateFormat':'YYYYMMDD'
            },

            'hidden' : {
              'p_type': 'F',
              '__lanid' : 2057
            },
            'dateFormat' : 'D MMMM YYYY',
            'strings' : {
              'retour': 'Roundtrip',
              'single': 'Single',
              'from': 'From',
              'to': 'To',
              'departure': 'Departure',
              'arrival': 'Arrival',
              'adults': 'Adults',
              'kids': 'Kids (2-11)',
              'babies': 'Babies (0-1)',
              'search': 'Search'
            }
          },
          'vliegwinkel-nl' : {
            'postURL' : 'http://www.vliegwinkel.nl/checkout/flightresults',
            'homepageURL' : '//www.vliegwinkel.nl',
            'autoCompleteURL' : '//api.vliegwinkel.nl/lookup/airports',
            'params' : {
              'triptype' : {
                'name' : 'oneway',
                'values' : [
                  0, 1
                ]
              },
              'dep_apcode' : 'out0_dep',
              'des_apcode' : 'out0_arr',
              'vertrekdate' : 'out0_date',
              'retourdate' : 'in0_date',
              'nr_adults' : 'adt',
              'nr_children' : 'chd',
              'nr_infant' : 'inf',
              'search' : 'Zoeken',
              'postDateFormat':'YYYYMMDD'
            },
            'hidden' : {
              'p_type': 'F',
              '__lanid' : 1031
            },
            'dateFormat' : 'DD-MM-YYYY',
            'strings' : {
              'retour': 'Retour',
              'single': 'Enkele reis',
              'from': 'Van',
              'to': 'Naar',
              'departure': 'Heen',
              'arrival': 'Terug',
              'adults': 'Volw.',
              'kids': 'Kinderen 2-11 jaar',
              'babies': 'Baby\'s 0-1 jaar',
              'search': 'Zoeken'
            }
          },
          'flugladen-at' : {
            'postURL' : 'http://www.flugladen.at/checkout/flightresults',
            'homepageURL' : '//www.flugladen.at',
            'autoCompleteURL' : '//api.flugladen.at/lookup/airports',
            'params' : {
              'triptype' : {
                'name' : 'oneway',
                'values' : [
                  0, 1
                ]
              },
              'dep_apcode' : 'out0_dep',
              'des_apcode' : 'out0_arr',
              'vertrekdate' : 'out0_date',
              'retourdate' : 'in0_date',
              'nr_adults' : 'adt',
              'nr_children' : 'chd',
              'nr_infant' : 'inf',
              'search' : 'Suchen',
              'postDateFormat':'YYYYMMDD'
            },
            'hidden' : {
              'p_type': 'F',
              '__lanid' : 1031
            },
            'dateFormat' : 'DD.MM.YYYY',
            'strings' : {
              'retour': 'Hin-und R�ckflug ',
              'single': 'Nur Hinflug',
              'from': 'Von',
              'to': 'Nach',
              'departure': 'Hinflug',
              'arrival': 'Ruckflug',
              'adults': 'Erwachsene',
              'kids': 'Kinder (2-11)',
              'babies': 'Kleinkinder (0-1)',
              'search': 'Suchen'
            }
          },
          'flugladen-de' : {
            'postURL' : 'http://www.flugladen.de/checkout/flightresults',
            'homepageURL' : '//www.flugladen.de',
            'autoCompleteURL' : '//api.flugladen.de/lookup/airports',
            'params' : {
              'triptype' : {
                'name' : 'oneway',
                'values' : [
                  0, 1
                ]
              },
              'dep_apcode' : 'out0_dep',
              'des_apcode' : 'out0_arr',
              'vertrekdate' : 'out0_date',
              'retourdate' : 'in0_date',
              'nr_adults' : 'adt',
              'nr_children' : 'chd',
              'nr_infant' : 'inf',
              'search' : 'Suchen',
              'postDateFormat':'YYYYMMDD'
            },
            'hidden' : {
              'p_type': 'F',
              '__lanid' : 1031
            },
            'dateFormat' : 'DD.MM.YYYY',
            'strings' : {
              'retour': 'Hin-und R�ckflug ',
              'single': 'Nur Hinflug',
              'from': 'Von',
              'to': 'Nach',
              'departure': 'Hinflug',
              'arrival': 'Ruckflug',
              'adults': 'Erwachsene',
              'kids': 'Kinder (2-11)',
              'babies': 'Kleinkinder (0-1)',
              'search': 'Suchen'
            }
          }
        },
        'maxPassengers' : 9,
        'prefillfromcode' : '',
        'prefillfrom' : '',
        'prefilltocode' : '',
        'prefillto' : '',
        'strings' : {
          'retour' : 'Retour',
          'single' : 'Enkele reis',
          'from' : 'Van',
          'to' : 'Naar',
          'departure' : 'Heen',
          'arrival' : 'Terug',
          'adults' : 'Volw.',
          'kids' : 'Kinderen 2-11 jaar',
          'babies' : 'Baby\'s 0-1 jaar',
          'search' : 'Zoeken'
        }
      }, options);

      this.views = [];
      this.render();
    },
    render: function() {
      _.each(this.views, function(v) {
        v.undelegateEvents();
      }, this);

      _.each($(this.options.element), function(element) {
        var options = $.extend(this.options, $(element).data());
        var v = new BannerView({'el': element});

        v.controller = this;
        v.options = options;
        v.render();
        this.views.push(v);

      }, this);
    },
    doPost: function(e, form) {

        e.preventDefault();

        var encodedPostURL = '';
        var theConfig = this.options.config[this.options.label];

        form.find('input, select').each(function (index, element) {
          var paramName = '';
          var pName = $(element).attr('name');
          var pValue = $(element).val();

          // convert dates back into into standard format.
          if ((typeof theConfig.params === "undefined") || (typeof theConfig.params[pName] === "undefined")){
            paramName = pName;
          } else {
            paramName = theConfig.params[pName];
          }
          if( (pName == 'vertrekdate'  || pName == 'retourdate' ) && ( pValue !== "undefined" || pValue.length > 6 ) ){
            console.log(theConfig.params.postDateFormat);
            pValue = moment(pValue, theConfig.dateFormat).format(theConfig.params.postDateFormat);

          }

          encodedPostURL += '&' + paramName + "=" + pValue;

        });
        if (this.options.affiliateurl) {
          window.open(form.attr('action') + encodeURIComponent(encodeURIComponent('?' + encodedPostURL)), '_blank');
        } else {
          window.open(form.attr('action') + '?' + encodedPostURL, '_blank');
        }

    },
    postURL: function() {
      if (this.options.affiliateurl) {
        return this.options.affiliateurl + encodeURIComponent(encodeURIComponent(this.options.config[this.options.label].postURL));
      }
      return this.options.config[this.options.label].postURL;
    },
    homepageURL: function() {
        return this.options.config[this.options.label].homepageURL;
    },
    getParam: function(paramName) {
      if (typeof this.options.config[this.options.label].params === "undefined"
        || typeof this.options.config[this.options.label].params[paramName] === "undefined") {
        return paramName;
      }
      return this.options.config[this.options.label].params[paramName];
    },

    getLabel: function(labelName) {
      if (typeof this.options.config[this.options.label].strings === "undefined"
          || typeof this.options.config[this.options.label].strings[labelName] === "undefined") {
        return labelName;
      }
      return this.options.config[this.options.label].strings[labelName];
    },

    getDateFormat: function() {
      if (typeof this.options.config[this.options.label].dateFormat === "undefined") {
        return this.options.dateFormat;
      }
      return this.options.config[this.options.label].dateFormat;
    },
    autoComplete: function(field, hiddenField, callback) {
      if (field.val().length >= 3) {

        /*
        $.getJSON(this.options.autoCompleteURL + '?callback=?&culture=' + this.options.lang + '&q=' + field.val(), {}, function(data) {
          callback(data);
        });
        */
        $.getJSON(this.options.config[this.options.label].autoCompleteURL + '?callback=&culture=' + this.options.lang + '&search=' + field.val(), {}, function(data) {
          callback(data);
        });

      }
      return this ;
    },
    setMaxPassengers: function(adults, kids, babies) {
      var maxTotal = this.options.maxPassengers;
      // No more babies than adults
      if(babies.val() > adults.val()) {
        babies.val(adults.val());
      }
      // Max kids
      var prevKids = kids.val();
      var maxKids = maxTotal-adults.val();
      kids.empty();
      for(var i=0;i<=maxKids;i++) {
        if((prevKids <= maxKids && i == prevKids) || (prevKids == maxKids && i == prevKids)) {
          kids.append('<option selected="selected">'+i+'</option>');
        } else {
          kids.append('<option>'+i+'</option>');
        }
      }
      // Max
      var prevBabies = babies.val();
      var maxBabies = maxTotal-(Number(adults.val())+Number(kids.val()));
      if(maxBabies > Number(adults.val())) {
        maxBabies = Number(adults.val());
      }
      babies.empty();
      for(i=0;i<=maxBabies;i++) {
        if((prevBabies < maxBabies && i == prevBabies) || (prevBabies == maxBabies && i == maxBabies)) {
          babies.append('<option selected="selected">'+i+'</option>');
        } else {
          babies.append('<option>'+i+'</option>');
        }
      }
    }
  });
});



