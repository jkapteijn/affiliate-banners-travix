define(['jquery', 'backbone', 'underscore', 'moment', 'pikaday', 'text!templates/vertical.html', 'text!templates/horizontal.html', 'css!../../../style.min.css'], function($, Backbone, _, Moment, Pikaday, Vertical, Horizontal, css) {
  return Backbone.View.extend({
    events: {
      'keyup #from, #to' : 'autoComplete',
      'change #adults, #kids, #babies' : 'setMaxPassengers',
      'click li.retour a' : 'setRetour',
      'click li.single a' : 'setSingle',
      'click .awesomplete li' : 'selectRow',
      'submit form' : 'submit'
    },
    horizontal: [
      'leaderboard',
      'banner'
    ],

    submit: function(e) {

      return this.controller.doPost(e, this.$("form"));
    },

    selectRow: function(e) {
      var field = $(e.target).data('field');
      var hiddenField = $(e.target).data('target');
      this.$(field).val($(e.target).text());
      this.$(hiddenField).val($(e.target).data('code'));
      this.getList($(field)).hide();
    },

    createList: function(element) {
      var list = $('<div class="awesomplete" data-for="'+element.attr('id')+'"><ul></ul></div>');
      list.insertAfter(element);
      list.hide();
      return list;
    },

    getList: function(element) {
      return this.$('[data-for="'+element.attr('id')+'"]');
    },

    autoComplete: function(e) {
      var _this = this;
      this.controller.autoComplete($(e.target), $($(e.target).data('target')), function(items) {
        var list = _this.getList($(e.target));
        list.find('ul').empty();

        items.forEach(function(row) {
            list.find('ul').append('<li data-code="'+row.Code+'" data-field="#'+$(e.target).attr('id')+'" data-target="'+$(e.target).data('target')+'">'+row.DisplayName+'</li>');
        });
        list.show();
      });
    },
    setMaxPassengers: function(e) {
      if (this.$('#kids').length)
        this.controller.setMaxPassengers(this.$('#adults'), this.$('#kids'), this.$('#babies'));
    },

    setRetour:function(e) {
      e.preventDefault();
      this.$('li').removeClass('active');
      $(e.target).parent('li').addClass('active');
      this.$('.retour-input').animate({opacity: 1}, 'fast');
      this.$('input[id="triptype"]').val('retour');
      return this;
    },

    setSingle:function(e) {
      e.preventDefault();
      this.$('li').removeClass('active');
      $(e.target).parent('li').addClass('active');
      this.$('.retour-input').animate({opacity: 0}, 'fast');
      this.$('input[id="triptype"]').val('single');
      return this;
    },

    initCalendars: function(elements) {
      var _this = this;
      _.each(elements, function(element) {
        var min = new Moment().add(this.controller.options.minNextDays, 'days');
        var max = new Moment().add(this.controller.options.maxNextDays, 'days');
        var picker = new Pikaday({
          field: element,
          format: this.controller.getDateFormat(),
          minDate: min.toDate(),
          maxDate: max.toDate(),
          firstDay: 1,
          i18n: {
              // previousMonth : '<i class="icon-left">prev</i>',
              // nextMonth     : '<i class="icon-right">next</i>',
              months        : ['Januari','Februari','Maart','April','Mei','Juni','Juli','Augustus','September','Oktober','November','December'],
              weekdays      : ['Zondag','Maandag','Dinsdag','Woensdag','Donderdag','Vrijdag','Zaterdag'],
              weekdaysShort : ['Zo','Ma','Di','Wo','Do','Vr','Za']
          }
        });
        $(picker.el).addClass(_this.controller.options.label);
      }, this);

      return this;
    },
    render: function() {
      if (_.indexOf(this.horizontal, this.controller.options.layout)  >= 0) {
        this.template = _.template(Horizontal);
      } else {
        this.template = _.template(Vertical);
      }
      this.$el.html(this.template(this.controller));
      this.initCalendars(this.$('input[data-type="calendar"]'));
      this.createList(this.$('#from'));
      this.createList(this.$('#to'));
      return this;
    }
  });
});
