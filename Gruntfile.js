module.exports = function(grunt) {

  // Configuration
  grunt.initConfig({
    pkg: grunt.file.readJSON('application-config.json'),
    sass: {
      dist: {
        files: {
          'style.css': 'assets/scss/style.scss'
        }
      }
    },

    watch: {
      html:{
        files: ['assets/js/templates/*.html'],
        tasks:['exec','replace']
      },
      css : {
        files: ['**/*.scss'],
        tasks: ['sass', 'cssmin', 'replace']
      },
      js : {
        files: ['assets/js/**/*.js'],
        tasks: ['exec','replace']
      }
    },

    cssmin: {
      target: {
        files: {
          'style.min.css': ['style.css']
        }
      }
    },

    exec: {
      build: {
        cmd: 'r.js.cmd -o build.js'
      }
    },
    replace: {
      replace_rootpath: {
        src: ['build/qssdk.js'],
        //dest: ['build/qssdk_rpl.js'],
        overwrite: true,
        replacements: [{
          from: 'http://[rootPath]',
          to: "<%= pkg.rootPath %>"
        }]
      }
    },

    compress: {
      main: {
        options: {
          archive: 'export/affiliates.zip'
        },
        files: [
          {cwd:'./', src: ['index.html', '.htaccess']},
          //{src: ['build/qssdk.js', 'build/require.js'], expand: true},
          {cwd:'build/', src: ['*.js'], expand:true, dest: ''},
          {cwd:'assets/', src: ['**/*'], expand: true, dest: 'assets'}
        ]
      }
    }
  });

  // Load tasks
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-exec');
  grunt.loadNpmTasks('grunt-text-replace');
  grunt.loadNpmTasks('grunt-contrib-compress');

  // Register tasks
  grunt.registerTask('default', ['sass', 'cssmin', 'watch', 'exec', 'replace','compress']);
  grunt.registerTask('export', ['sass', 'cssmin', 'exec', 'replace','compress']);
}
